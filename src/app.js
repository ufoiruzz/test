import React from "react";
import ReactDOM from "react-dom";
import Navigation from './navigation/navigation';
import Routes from './routes/routes'

class App extends React.Component {
	render() {
		return(
			<div>
				<Routes />
			</div>
			);
		}
	}

export default App;