import React from "react";
import ReactDOM from "react-dom";
import { NavLink } from 'react-router-dom';
import { Grid,Col } from 'react-bootstrap';


class Navigation extends React.Component {
	render() {
		return(
			<Grid>
				<Col xs={3}><NavLink to='/'>Home</NavLink></Col>
				<Col xs={3}><NavLink to='/users'>User</NavLink></Col>
				<Col xs={3}><NavLink to='/gallery'>Gallery</NavLink></Col>
			</Grid>
		);
	}
}

export default Navigation;