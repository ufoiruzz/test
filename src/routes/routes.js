import React from "react";
import ReactDOM from "react-dom";
import { Switch,Route } from "react-router-dom";

// pages 
import Home from "../pages/home";
import Post from "../pages/post";
import Comment from "../pages/comment";
import Album from "../pages/album";
import AlbumDetail from "../pages/albumdetail";

class Routes extends React.Component {
	render() {
		return(
			<Switch>
				<Route exact path='/' component={ Home }/>
				<Route path='/post/:post' component={ Post }/>
				<Route path='/comment/:comment' component={ Comment }/>
				<Route path='/album/:user' component={ Album }/>
				<Route exact path='/album-detail/:album' component={ AlbumDetail }/>
			</Switch>
			);
	}
}

export default Routes;