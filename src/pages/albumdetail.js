import React from "react";
import ReactDOM from "react-dom";
import { NavLink } from 'react-router-dom';
import { Grid,Col,Panel,Button,ButtonGroup,Well,Modal } from 'react-bootstrap';

class AlbumDetail extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			album: this.props.match.params.album,
			photo: [],
			detail:'',
			info:'',
			popup:"static-modal"
		};
		this.openModal = this.openModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
	}
	componentDidMount() {
		fetch('https://jsonplaceholder.typicode.com/photos/?albumId='+this.state.album)
		.then(res => res.json())
		.then(data => this.setState({ photo:data }));
	}
	openModal(event){
		const image = event.target.attributes.getNamedItem('data-image').value;
		const title = event.target.attributes.getNamedItem('data-info').value;
		this.setState({
			popup: "static-modal active",
			detail:image,
			info: title
		})
	}
	closeModal() {
		this.setState({
			popup: "static-modal",
			detail:"",
			info: ""
		})
	}
	render() {
		return(
			<Grid>
				{/* ==== POP UP ADDPOST ==== */}
				<div className={this.state.popup}>
				  <Modal.Dialog>
				    <Modal.Header>
				      <Modal.Title>{this.state.info}</Modal.Title>
				    </Modal.Header>
				    	 <Modal.Body>
				    		<img src={this.state.detail} />
						 </Modal.Body>
						 <Modal.Footer>
					      <Button type="button" onClick={this.closeModal}>Close</Button>
					    </Modal.Footer>
				  </Modal.Dialog>
				</div>

				{this.state.photo.map((data,i) =>
				<Panel xs={12} key={data.id}>
					<Panel.Heading>
						<Col sx={12}>
						<img src={data.thumbnailUrl} />
						</Col>
						{data.title}
					</Panel.Heading>
					<Panel.Body>
						<Col xs={12}>
						<Button className='col-xs-12' bsStyle="primary" data-image={data.url} data-info={data.title} onClick={this.openModal}>
							lihat detail foto
						</Button>
						</Col>

					</Panel.Body>
				</Panel>
				)}
			</Grid>
			);
	}
}

export default AlbumDetail;