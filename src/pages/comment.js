import React from "react";
import ReactDOM from "react-dom";
import { NavLink } from 'react-router-dom';
import { Grid,Col,Panel,Button,ButtonGroup,Well,Modal,FieldGroup,FormGroup,FormControl,ControlLabel } from 'react-bootstrap';

class Comment extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			post: this.props.match.params.comment,
			comments:[],
			popup: "static-modal",
			popupedit:"static-modal",
			addcomment: {
				name: "",
				body:"",
				id:""
			},
			editcomment: {
				name: "",
				body:"",
				id:""
			}
		};
		this.openModal = this.openModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.addComment = this.addComment.bind(this);
		this.getValue = this.getValue.bind(this);

		this.editComment = this.editComment.bind(this);
		this.deletePost = this.deletePost.bind(this);
		this.openEditModal = this.openEditModal.bind(this);
		this.closeEditModal = this.closeEditModal.bind(this);
		this.getValueEdit = this.getValueEdit.bind(this);
	}
	componentDidMount() {
		fetch('https://jsonplaceholder.typicode.com/comments/?postId='+this.state.post)
		.then(res => res.json())
		.then(data => this.setState({ comments:data }));
	}

	deletePost(event) {
		const value = event.target.value;
		const data = this.state.comments;
		data.splice(value,1);
		this.setState({ comments:data });
	}

	getValueEdit(event) {
		const target = event.target;
	    const name = target.name;
	    const value = target.value;
	    const data = this.state.editcomment;
	    data[name] = value;
	   	this.setState({
      		editcomment: data
    	});

	}

	openModal() {
		this.setState({
			popup: "static-modal active"
		})
	}
	closeModal() {
		this.setState({
			popup: "static-modal",
			addcomment: {
				name: "",
				body:"",
				id:""
			}
		})
	}

	getValue(event) {
		const target = event.target;
	    const name = target.name;
	    const value = target.value;
	    const data = this.state.addcomment;
	    data[name] = value;
	   	this.setState({
      		addcomment: data
    	});
	}

	addComment(event) {
		let throwId;
		const post = this.state.comments;
		const add = this.state.addcomment;
		const leng = post.length - 1;
		const maxid = post[leng].id;
		const minid = post[0].id;

		if(maxid > minid) {
			throwId = maxid + 1;
		} else {
			throwId = minid + 1;
		}

	    add.id = throwId;
		post.unshift(add);
		this.closeModal();
		event.preventDefault();
	}



	openEditModal(event) {
		const target = parseInt(event.target.value);
		const grab = this.state.comments.find(item => item.id === target);
		this.setState({
			popupedit: "static-modal active",
			editcomment:{
				'name':grab.name,
				'body':grab.body,
				'id':grab.id
			}
		})
	}

	closeEditModal() {
		this.setState({
			popupedit: "static-modal",
			editcomment: {
				name: "",
				body:"",
				id:""
			}
		})
	}

	editComment(event) {
		const target = this.state.editcomment.id;
		const grab = this.state.comments.find(item => item.id === target);

		// edit value
		grab.name = this.state.editcomment.name;
		grab.body = this.state.editcomment.body;
		
		this.closeEditModal();
		event.preventDefault();
	}


	render() {
		return(
			<Grid>

				{/* ==== POP UP ADDPOST ==== */}
				<div className={this.state.popup}>
				  <Modal.Dialog>
				    <Modal.Header>
				      <Modal.Title>Tambah Post</Modal.Title>
				    </Modal.Header>
				    	<form onSubmit={this.addComment}>
					    	 <Modal.Body>
					    		<FormGroup>
							      <ControlLabel>Judul</ControlLabel>
							     <FormControl required componentClass="input" name='name' placeholder="textarea" value={this.state.addcomment.name} onChange={this.getValue} />
							    </FormGroup>
					    		<FormGroup>
							      <ControlLabel>Post</ControlLabel>
							      <FormControl name='body' componentClass="textarea" placeholder="textarea" value={this.state.addcomment.body} onChange={this.getValue} />
							    </FormGroup>
							 </Modal.Body>
							 <Modal.Footer>
						      <Button type="button" onClick={this.closeModal}>Close</Button>
						      <Button type="submit" bsStyle="primary">Save changes</Button>
						    </Modal.Footer>
				    	</form>
				  </Modal.Dialog>
				</div>

				{/* ==== POP UP edit ==== */}
				<div className={this.state.popupedit}>
				  <Modal.Dialog>
				    <Modal.Header>
				      <Modal.Title>Edit Post</Modal.Title>
				    </Modal.Header>
				    	<form onSubmit={this.editComment}>
					    	 <Modal.Body>
					    		<FormGroup>
							      <ControlLabel>Nama</ControlLabel>
							     <FormControl required componentClass="input" name='name' placeholder="textarea" value={this.state.editcomment.name} onChange={this.getValueEdit} />
							    </FormGroup>
					    		<FormGroup>
							      <ControlLabel>Komen</ControlLabel>
							      <FormControl name='body' componentClass="textarea" placeholder="textarea" value={this.state.editcomment.body} onChange={this.getValueEdit} />
							    </FormGroup>
							 </Modal.Body>
							 <Modal.Footer>
						      <Button type="button" onClick={this.closeEditModal}>Close</Button>
						      <Button type="submit" bsStyle="primary">Save changes</Button>
						    </Modal.Footer>
				    	</form>
				  </Modal.Dialog>
				</div>

				<Grid>
					<Well>
						<Button bsStyle="success" onClick={this.openModal}>Add Comment</Button>
					</Well>
				</Grid>

				{this.state.comments.map((data,i) => 
				<Panel xs={12} key={data.id}>
					<Panel.Heading>
						{ data.name }
					</Panel.Heading>
					<Panel.Body>
						<Col xs={12}>
							<p>{data.body}</p>
						</Col>
						<Col xs={6}>
							<ButtonGroup>
							  <Button value={data.id} onClick={this.openEditModal}>Edit</Button>
							  <Button onClick={this.deletePost} value={i}>Hapus</Button>
							</ButtonGroup>
						</Col>
					</Panel.Body>
				</Panel>
				)}
			</Grid>
			);
		}
	}

export default Comment;