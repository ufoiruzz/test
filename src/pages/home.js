import React from "react";
import ReactDOM from "react-dom";
import { NavLink } from 'react-router-dom';
import { Grid,Col,Button,Panel } from 'react-bootstrap';

class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			users: []
		};
	}
	componentDidMount() {
		fetch('https://jsonplaceholder.typicode.com/users')
		.then(res => res.json())
		.then(data => this.setState({ users:data }));

	}
	render() {
		return(
			<Grid>
			{this.state.users.map((data,i) =>
				<Panel xs={12} key={data.id}>
					<Panel.Heading>
						Nama : { data.name }
					</Panel.Heading>
					<Panel.Body>
						<Col xs={6}>
						<NavLink to= { '/post/' + data.id }>
						<Button bsStyle="primary">
							lihat post
						</Button>
						</NavLink>
						</Col>

						<Col xs={6}>
						<NavLink to= { '/album/' + data.id }>
						<Button bsStyle="primary">
							lihat album
						</Button>
						</NavLink>
						</Col>

					</Panel.Body>
				</Panel>
				)}
			</Grid>
			);
	}
}

export default Home;