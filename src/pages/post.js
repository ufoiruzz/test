import React from "react";
import ReactDOM from "react-dom";
import { NavLink } from 'react-router-dom';
import { Grid,Col,Panel,Button,ButtonGroup,Well,Modal,FieldGroup,FormGroup,FormControl,ControlLabel } from 'react-bootstrap';

class Posts extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			user: this.props.match.params.post,
			popup:"static-modal",
			popupedit:"static-modal",
			posts:[],
			addpost:{
				'title': '',
				'body':'',
				'id':''
			},
			editpost:{
				'title':'',
				'body':'',
				'id':''
			}
		};
		this.deletePost = this.deletePost.bind(this);
		this.openModal = this.openModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.getValue = this.getValue.bind(this);
		this.addPost = this.addPost.bind(this);

		this.openEditModal = this.openEditModal.bind(this);
		this.closeEditModal = this.closeEditModal.bind(this);
		this.editPost = this.editPost.bind(this);
		this.getValueEdit = this.getValueEdit.bind(this);
	}
	componentDidMount() {
		fetch('https://jsonplaceholder.typicode.com/posts/?userId='+this.state.user)
		.then(res => res.json())
		.then(data => this.setState({ posts:data }));
	}

	deletePost(event) {
		const value = event.target.value;
		const data = this.state.posts;
		data.splice(value,1);
		this.setState({ posts:data });
	}

	getValue(event) {
		const target = event.target;
	    const name = target.name;
	    const value = target.value;
	    const data = this.state.addpost;
	    data[name] = value;
	   	this.setState({
      		addpost: data
    	});
	}

	getValueEdit(event) {
		const target = event.target;
	    const name = target.name;
	    const value = target.value;
	    const data = this.state.editpost;
	    data[name] = value;
	   	this.setState({
      		editpost: data
    	});

	}

	openModal() {
		this.setState({
			popup: "static-modal active"
		})
	}

	openEditModal(event) {
		const target = parseInt(event.target.value);
		const grab = this.state.posts.find(item => item.id === target);
		this.setState({
			popupedit: "static-modal active",
			editpost:{
				'title':grab.title,
				'body':grab.body,
				'id':grab.id
			}
		})
	}

	closeModal() {
		this.setState({
			popup: "static-modal",
			addpost: {
				title: "",
				body:"",
				id:""
			}
		})
	}

	closeEditModal() {
		this.setState({
			popupedit: "static-modal",
			editpost: {
				title: "",
				body:"",
				id:""
			}
		})
	}

	addPost(event) {
		let throwId;
		const post = this.state.posts;
		const add = this.state.addpost;
		const leng = post.length - 1;

		const maxid = post[leng].id;
		const minid = post[0].id;

		if(maxid > minid) {
			throwId = maxid + 1;
		} else {
			throwId = minid + 1;
		}

		console.log(throwId);
	 	add.id = throwId;
		post.unshift(add);
		console.log(post);
		this.closeModal();
		event.preventDefault();
	}

	editPost(event) {
		const target = this.state.editpost.id;
		const grab = this.state.posts.find(item => item.id === target);

		// edit value
		grab.title = this.state.editpost.title;
		grab.body = this.state.editpost.body;
		
		this.closeEditModal();
		event.preventDefault();
	}

	render() {
		return(
			<Grid>

				{/* ==== POP UP ADDPOST ==== */}
				<div className={this.state.popup}>
				  <Modal.Dialog>
				    <Modal.Header>
				      <Modal.Title>Tambah Post</Modal.Title>
				    </Modal.Header>
				    	<form onSubmit={this.addPost}>
					    	 <Modal.Body>
					    		<FormGroup>
							      <ControlLabel>Judul</ControlLabel>
							     <FormControl required componentClass="input" name='title' placeholder="textarea" value={this.state.addpost.title} onChange={this.getValue} />
							    </FormGroup>
					    		<FormGroup>
							      <ControlLabel>Post</ControlLabel>
							      <FormControl name='body' componentClass="textarea" placeholder="textarea" value={this.state.addpost.body} onChange={this.getValue} />
							    </FormGroup>
							 </Modal.Body>
							 <Modal.Footer>
						      <Button type="button" onClick={this.closeModal}>Close</Button>
						      <Button type="submit" bsStyle="primary">Save changes</Button>
						    </Modal.Footer>
				    	</form>
				  </Modal.Dialog>
				</div>

				{/* ==== POP UP edit ==== */}
				<div className={this.state.popupedit}>
				  <Modal.Dialog>
				    <Modal.Header>
				      <Modal.Title>Edit Post</Modal.Title>
				    </Modal.Header>
				    	<form onSubmit={this.editPost}>
					    	 <Modal.Body>
					    		<FormGroup>
							      <ControlLabel>Judul</ControlLabel>
							     <FormControl required componentClass="input" name='title' placeholder="textarea" value={this.state.editpost.title} onChange={this.getValueEdit} />
							    </FormGroup>
					    		<FormGroup>
							      <ControlLabel>Post</ControlLabel>
							      <FormControl name='body' componentClass="textarea" placeholder="textarea" value={this.state.editpost.body} onChange={this.getValueEdit} />
							    </FormGroup>
							 </Modal.Body>
							 <Modal.Footer>
						      <Button type="button" onClick={this.closeEditModal}>Close</Button>
						      <Button type="submit" bsStyle="primary">Save changes</Button>
						    </Modal.Footer>
				    	</form>
				  </Modal.Dialog>
				</div>

				<Grid>
					<Well>
						<Button bsStyle="success" onClick={this.openModal}>Add Post</Button>
					</Well>
				</Grid>

			{this.state.posts.map((data,i) => 
				<Panel xs={12} key={data.id}>
					<Panel.Heading>
						{ data.title }
					</Panel.Heading>
					<Panel.Body>
						<Col xs={12}>
							<p>{data.body}</p>
						</Col>
						<Col xs={6}>
							{ data.userId ? 
								<NavLink to= { '/comment/' + data.id }>
								<Button bsStyle="primary">
									lihat komen
								</Button>
								</NavLink>
								:
								""
							}
						</Col>
						<Col xs={6}>
							<ButtonGroup>
							  <Button value={data.id} onClick={this.openEditModal}>Edit</Button>
							  <Button onClick={this.deletePost} value={i}>Hapus</Button>
							</ButtonGroup>
						</Col>
					</Panel.Body>
				</Panel>
				)}
			</Grid>
			);
		}
	}

export default Posts;