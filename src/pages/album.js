import React from "react";
import ReactDOM from "react-dom";
import { NavLink } from 'react-router-dom';
import { Grid,Col,Button,Panel } from 'react-bootstrap';

class Album extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			user: this.props.match.params.user,
			album: []
		};
	}
	componentDidMount() {
		fetch('https://jsonplaceholder.typicode.com/albums/?userId='+this.state.user)
		.then(res => res.json())
		.then(data => this.setState({ album:data }));
	}
	render() {
		return(
			<Grid>
				{this.state.album.map((data,i) =>
				<Panel xs={12} key={data.id}>
					<Panel.Heading>
						Nama Album: { data.title }
					</Panel.Heading>
					<Panel.Body>
						<Col xs={12}>
						<NavLink to= { '/album-detail/' + data.id }>
						<Button className='col-xs-12' bsStyle="primary">
							lihat foto
						</Button>
						</NavLink>
						</Col>

					</Panel.Body>
				</Panel>
				)}
			</Grid>
			);
	}
}

export default Album;